package data

import (
	"encoding/json"
	"fmt"
	"io"
	"time"
)

type Coin struct {
	ID        int     `json:"id"`
	Name      string  `json:"name"`
	Symbol    string  `json:"symbol"`
	Price     float64 `json:"price"`
	Change24h float64 `json:"24h"`
	Volume    float64 `json:"max_volume"`
	CreatedOn string  `json:"-"`
	UpdatedOn string  `json:"-"`
	Deletedon string  `json:"-"`
}

type Coins []*Coin

func (c *Coins) ToJSON(w io.Writer) error {
	e := json.NewEncoder(w)
	return e.Encode(c)
}

func (c *Coin) FromJSON(r io.Reader) error {
	e := json.NewDecoder(r)
	return e.Decode(c)
}

func GetCoins() Coins {
	return coinList
}

func AddCoin(c *Coin) {
	c.ID = getNextID()
	coinList = append(coinList, c)
}

func UpdateCoin(id int, c *Coin) error {
	_, pos, err := findCoinByID(id)
	if err != nil {
		return err
	}

	c.ID = id
	coinList[pos] = c

	return nil
}

var ErrCoinNotFound = fmt.Errorf("Coin not found")

func findCoinByID(id int) (*Coin, int, error) {
	for i, c := range coinList {
		if c.ID == id {
			return c, i, nil
		}
	}

	return nil, -1, ErrCoinNotFound
}

func getNextID() int {
	cp := coinList[len(coinList)-1]
	return cp.ID + 1
}

var coinList = []*Coin{
	&Coin{
		ID:        1,
		Name:      "Bitcoin",
		Symbol:    "BTC",
		Price:     44013.49933770118,
		Change24h: -8.15321457,
		Volume:    21000000,
		CreatedOn: time.Now().UTC().String(),
		UpdatedOn: time.Now().UTC().String(),
	},
	&Coin{
		ID:        2,
		Name:      "Ethereum",
		Symbol:    "ETH",
		Price:     1364.511290429181,
		Change24h: -6.60928273,
		Volume:    0,
		CreatedOn: time.Now().UTC().String(),
		UpdatedOn: time.Now().UTC().String(),
	},
}
