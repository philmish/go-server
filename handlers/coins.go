package handlers

import (
	"gitlab.com/philmish/go-server/data"
	"log"
	"net/http"
	"regexp"
	"strconv"
)

type Coins struct {
	l *log.Logger
}

func NewCoins(l *log.Logger) *Coins {
	return &Coins{l}
}

func (c *Coins) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	// if request method is Get return all coins
	if r.Method == http.MethodGet {
		c.getCoins(w, r)
		return
	}

	if r.Method == http.MethodPost {
		c.postCoin(w, r)
	}

	if r.Method == http.MethodPut {
		c.l.Println("PUT")

		// check URI for the id
		re := regexp.MustCompile(`/([0-9]+)`)
		g := re.FindAllStringSubmatch(r.URL.Path, -1)

		if len(g) != 1 {
			http.Error(w, "Invalid URI", http.StatusBadRequest)
			return
		}

		if len(g[0]) != 2 {
			c.l.Println("URI not identifiable")
			http.Error(w, "Invaild URI", http.StatusBadRequest)
			return
		}

		idString := g[0][1]
		id, err := strconv.Atoi(idString)
		if err != nil {
			http.Error(w, "Invalid URI", http.StatusBadRequest)
			return
		}

		c.updateCoin(id, w, r)
		return
	}

	// catch all other method calls
	w.WriteHeader(http.StatusMethodNotAllowed)
}

func (c *Coins) getCoins(w http.ResponseWriter, r *http.Request) {
	lp := data.GetCoins()
	err := lp.ToJSON(w)
	if err != nil {
		http.Error(w, "Unable to marshal json", http.StatusInternalServerError)
	}
}

func (c *Coins) postCoin(w http.ResponseWriter, r *http.Request) {
	c.l.Println("Handle POST Coins")

	coin := &data.Coin{}

	err := coin.FromJSON(r.Body)
	if err != nil {
		http.Error(w, "Unable to unmarshal json", http.StatusBadRequest)
	}

	data.AddCoin(coin)
}

func (c *Coins) updateCoin(id int, w http.ResponseWriter, r *http.Request) {
	c.l.Println("Handle PUT request")

	coin := &data.Coin{}

	err := coin.FromJSON(r.Body)
	if err != nil {
		http.Error(w, "Unable to unmarshal json", http.StatusBadRequest)
	}

	err = data.UpdateCoin(id, coin)
	if err == data.ErrCoinNotFound {
		http.Error(w, "Coin not found", http.StatusInternalServerError)
		return
	}
}
