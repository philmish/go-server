package main

import (
	"context"
	"fmt"
	"gitlab.com/philmish/go-server/handlers"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func main() {
	l := log.New(os.Stdout, "api", log.LstdFlags)
	hh := handlers.NewHello(l)
	ch := handlers.NewCoins(l)
	gb := handlers.NewGoodbye(l)

	nm := http.NewServeMux()
	nm.Handle("/", ch)
	nm.Handle("/bye", gb)
	nm.Handle("/hello", hh)

	s := &http.Server{
		Addr:         ":5008",
		Handler:      nm,
		IdleTimeout:  120 * time.Second,
		ReadTimeout:  3 * time.Second,
		WriteTimeout: 3 * time.Second,
	}

	go func() {
		fmt.Printf("Staring API Server on Port %s", s.Addr)

		err := s.ListenAndServe()
		if err != nil {
			l.Fatal(err)
		}
	}()

	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, os.Interrupt)
	signal.Notify(sigChan, os.Kill)

	sig := <-sigChan
	l.Println("Recieved terminate, graceful shutdown", sig)

	tc, _ := context.WithTimeout(context.Background(), 30*time.Second)
	s.Shutdown(tc)
}
